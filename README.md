# Symfony avec Docker

## Docker

1. Lancer `apt install docker-compose` pour utiliser les dépôts officiels
2. Lancer `sudo apt-get remove docker docker-engine docker.io containerd runc` pour désinstaller les versions précédentes de Docker.
3. Lancer `sudo apt-get update` pour mettre à jour APT.
4. Lancer `sudo apt-get install \ apt-transport-https \ ca-certificates \ curl \ software-properties-common` pour installer les paquets permettant à APT d'utiliser un serveur HTTPS de dépôt.
5. Lancer `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg` pour ajouter la clé GPG du site de Docker.

### Docker CE
1. Lancer `echo \ "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \ $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null` pour pointer vers le dépôt de la version "stable" de Docker CE.
2. Lancer `sudo apt-get update` pour mettre à jour l'index APT.
3. Lancer `sudo apt-get install docker-ce docker-ce-cli containerd.io` pour installer la dernière version de Docker Engine et containerd.

## Symfony

1. Une fois que Docker est bien installé, 
2. Lancer `sudo docker-compose build --pull --no-cache` pour créer les images
3. Lancer `sudo docker-compose up`
4. Ouvrir `https://localhost` dans votre navigateur et acceptez le message d'erreur
5. Lancer `sudo docker-compose down --remove-orphans` pour arrêter le containeur
